#ifndef RANDOM_GENRATOR_HPP
#define RANDOM_GENRATOR_HPP

class RandomGenerator {
public:
	RandomGenerator(long seed);
	long rand();
private:
	long seed;
	bool firstTime;
};

#endif