#include "Client.hpp"
#include <iostream>
#include <string>

int isNumber(const char *s) {
	int i = 0;

	while (s[i] != 0) {
		if (s[i] < '0' || s[i] > '9') {
			return 0;
		}
		i++;
	}

	return 1;
}

void validatePort(std::string port) {
	int64_t port_int;
	try {
		port_int = std::stoll(port);
		if (port_int < 0 || port_int > 65535|| !isNumber(port.c_str())) {
			fprintf(stderr, "Error: port has to be in range 0-65535\n");
			exit(1);
		}

	} catch (std::exception &e) {
		fprintf(stderr, "Error: -p has to be a number \n");
		exit(1);
	}
}

std::string readName(char *arg) {
	std::string argument;
	try {
		argument = std::string(arg);	
	} catch(std::exception &e) {
		fprintf(stderr, "Error: can't read argument\n");
		exit(1);
	}

	return argument;
	//walidacja readName?

}

void readHost(char *arg, std::string *host_name, std::string *host_port) {
		std::string argument;
		try {
			argument = std::string(arg);	
		} catch(std::exception &e) {
			fprintf(stderr, "Error: can't read argument\n");
			exit(1);
		}

		int last_colon = argument.find_last_of(":");
		int first_colon = argument.find_first_of(":");

		if (first_colon == -1) { //nie podano portu (port domyslny) ipv4
			*host_name = argument;
			*host_port = "";
		} else if (first_colon != last_colon) { //ipv6?
			*host_name = argument;
			*host_port = "";
		} else { //oba- ipv4?
			*host_name = argument.substr(0, first_colon);
			*host_port = argument.substr(last_colon + 1, argument.length());
		}
}


int main(int argc, char *argv[]) {

	std::string name = "";
	std::string server_ip = "";
	std::string server_port = "";
	std::string gui_ip = "";
	std::string gui_port = "";

	if (!(argc == 3 || argc == 4)) {
		fprintf(stderr, "Usage: %s player_name game_server_host[:port] [ui_server_host[:port]]\n", argv[0]);
		exit(1);
	}

	name = readName(argv[1]);
	readHost(argv[2], &server_ip, &server_port);

	if (server_port == "") {
		server_port = Client::DEFAULT_SERVER_PORT;
	}

	if (argc == 4) {
		readHost(argv[3], &gui_ip, &gui_port);
		if (gui_port == "") {
			gui_port = Client::DEFAULT_GUI_PORT;
		}
	} else {
		gui_ip = Client::DEFAULT_GUI_HOST;
		gui_port = Client::DEFAULT_GUI_PORT;
	}

	validatePort(gui_port);
	validatePort(server_port);
	std::cout << "name " << name << " server " << server_ip
		<< " port " << server_port << " gui " << gui_ip << " port " << gui_port << std::endl;
	
	Client client(name, server_ip, server_port, gui_ip, gui_port);
	client.run();
	return 0;
}
