#include "Player.hpp"
#include <iostream>
#include <cmath>

Player::Player()
: name("")
, x(0)
, y(0)
, direction(0)
, alive(false) {}

Player::Player(std::string name, double x, double y, int direction)
: name(name)
, x(x)
, y(y)
, direction(direction)
, turn_direction(0)
, alive(true)
 {}

void Player::updateDirection(int turning_speed) {
	if (!turn_direction) return;

	if (turn_direction > 0) {
		direction = (direction + turning_speed) % 360;
	} else {
		direction -= turning_speed;
		if (direction < 0) {
			direction = 360 - direction;
		}
	}
}

void Player::setAlive(bool status) {
	alive = status;
}

bool Player::isAlive() {
	return alive;
}

int Player::getX() {
	if (x < 0) {
		return -1;
	}

	return (int) x;
}

int Player::getY() {
	if (y < 0) {
		return -1;
	}
	return (int) y;
}

void Player::setTurnDirection(int turn) {
	turn_direction = turn;
}

bool Player::moveForward() {
	double radians = direction * M_PI / 180.0;
	double dx = std::cos(radians) * 1;
	double dy = std::sin(radians) * 1;
	int old_x = getX();
	int old_y = getY();

	x += dx;
	y += dy;

	//czy zmienily sie wspolrzedne
	if (getX() != old_x || getY() != old_y) {
		return true;
	} else {
		return false;
	}
}