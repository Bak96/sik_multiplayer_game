#ifndef DATAGRAM_HPP
#define DATAGRAM_HPP
#include <endian.h>
#include <inttypes.h>
#include <string>

struct Data {
	int len;
	char *data;
};

struct ClientDatagram {
	uint64_t session_id;
	int8_t turn_direction;
	uint32_t next_expected_event_no;
	std::string player_name;
};

#endif