#ifndef CLIENTOBJECT_HPP
#define CLIENTOBJECT_HPP
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string>

class ClientObject {
public:
	ClientObject(struct sockaddr_in6 addr, uint64_t session_id, std::string &player_name);
	struct sockaddr_in6 &getAddr();
	bool sameAddr(struct sockaddr_in6 addr);
	bool isReady();
	void setReady(bool r);
	std::string &getName();
	uint32_t getExpectedEventNo();
	void setExpectedEventNo(uint32_t expected_no);
	void updateContactTime();
	time_t getContactTime();
	uint64_t getSessionId();
	void setSessionId(uint64_t id);
	void setName(std::string name);
	bool isPlaying();
	void setPlaying(bool playing);

	uint32_t getSendingFrom();
	void setSendingFrom(uint32_t no);
	void startSending();

private:
	struct sockaddr_in6 addr;
	time_t contact_time;
	bool ready;
	bool is_playing;
	uint64_t session_id;
	uint32_t next_expected_event_no;
	std::string player_name;

	uint32_t sendingFrom;
};

#endif