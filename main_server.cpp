#include "Server.hpp"
#include <unistd.h>
#include <exception>
#include <iostream>


//return error
int isNumber(const char *s) {
	int i = 0;

	while (s[i] != 0) {
		if (s[i] < '0' || s[i] > '9') {
			return 0;
		}
		i++;
	}

	return 1;
}

int main(int argc, char *argv[]) {
	int c;
	int width = Server::DEFAULT_WIDTH;
	int height = Server::DEFAULT_HEIGHT;
	int rounds_per_sec = Server::DEFAULT_ROUNDS_PER_SEC;
	int turning_speed = Server::DEFAULT_TURNING_SPEED;
	std::string port = Server::DEFAULT_PORT;
	uint64_t seed = time(NULL);
	int64_t port_int;

	std::string argument;

	while((c = getopt(argc, argv, "W:H:p:s:t:r:")) != -1) {
		try {
			argument = std::string(optarg);	

		} catch(std::exception &e) {
			fprintf(stderr, "Error: can't read argument\n");
			return 1;
		}

		switch (c) {
		case 'W':
			try {
				width = std::stoi(argument);
				if (width <= 0 || !isNumber(argument.c_str())) {
					fprintf(stderr, "-W has to be a positive number\n");
					exit(1);
				}
			} catch (std::exception &e) {
				fprintf(stderr, "Error: -W has to be a number \n");
				exit(1);
			}
			
			break;
		case 'H':
			try {
				height = std::stoi(argument);
				if (width <= 0 || !isNumber(argument.c_str())) {
					fprintf(stderr, "-W has to be a positive number\n");
					exit(1);
				}
			} catch (std::exception &e) {
				fprintf(stderr, "Error: -W has to be a number \n");
				exit(1);
			}
			break;
		case 'p':
			port = argument;
			try {
				port_int = std::stoll(port);
				if (port_int < 0 || port_int > 65535|| !isNumber(argument.c_str())) {
					fprintf(stderr, "Error: port has to be in range 0-65535\n");
					exit(1);
				}

			} catch (std::exception &e) {
				fprintf(stderr, "Error: -p has to be a number \n");
				exit(1);
			}
			break;
		case 's':
			try {
				rounds_per_sec = std::stoi(argument);
				if (rounds_per_sec <= 0 || !isNumber(argument.c_str())) {
					fprintf(stderr, "Error -s has to be a positive number\n");
					exit(1);
				}
			} catch (std::exception &e) {
				fprintf(stderr, "Error: -s has to be a number \n");
				exit(1);
			}	
			break;
		case 't':
			try {
				turning_speed = std::stoi(argument);	
				turning_speed = turning_speed % 360;
				if (turning_speed <= 0 || !isNumber(argument.c_str())) {
					fprintf(stderr, "Error: turning_speed has to be a positive number");
					exit(1);
				}
			} catch (std::exception &e) {
				fprintf(stderr, "Error -t has to be a number\n");
				exit(1);
			}
			
			break;
		case 'r':
			try {
				if (argument[0] == '-' || !isNumber(argument.c_str())) {
					fprintf(stderr, "Error: seed has to be a positive number\n");
					exit(1);	
				}

				seed = std::stoull(argument);	
			} catch (std::exception &e) {
				fprintf(stderr, "Error: seed has to be a positive number\n");
				exit(1);
			}
			
			break;
		case '?':
			if (optopt == 'W' || optopt == 'H' || optopt == 'p'
				|| optopt == 's' || optopt == 't' || optopt == 'r') {
				fprintf(stderr, "Option -%c requires an argument .\n", optopt);
			} else if (isprint(optopt)) {
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);
			} else {
				fprintf(stderr, "Unknown option character '\\x%x'", optopt);
			}
			return 1;
		default:
			fprintf(stderr, "ERROR parsing commands\n");
			return 1;
		}

	}

	if (optind != argc) {
		fprintf(stderr, "Error: unknown arguments\n");
		exit(1);
	}

	std::cout << "width: " << width << " height: " << height
		<< " port: " << port << " rps " << rounds_per_sec
		<< " turning: " << turning_speed << " seed " << seed << std::endl;
	Server server(width, height, port, rounds_per_sec, turning_speed, seed);
	server.run();
}