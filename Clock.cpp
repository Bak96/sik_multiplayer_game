#include "Clock.hpp"
#include <sys/time.h>

Clock::Clock() {
	start();
}

void Clock::start() {
	start_time = getTimeInMillis();
}

void Clock::restart() {
	start();
}

long Clock::elapsed() {
	return getTimeInMillis() - start_time;
}

long Clock::getTimeInMillis() {
	struct timeval t;
	gettimeofday(&t, 0);
	return ((t.tv_sec) * 1000 + t.tv_usec/1000.0);
}
