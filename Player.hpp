#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>

class Player {
public:
	Player();
	Player(std::string name, double x, double y, int direction);
	void setAlive(bool status);
	bool isAlive();
	void updateDirection(int turning_speed);
	void setTurnDirection(int turn);
	bool moveForward();

	int getX();
	int getY();

private:
	std::string name;
	double x;
	double y;
	int direction;
	int turn_direction; //1 prawo -1 lewo, 0 brak zmiany
	bool alive;
};

#endif