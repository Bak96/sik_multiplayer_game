#ifndef CLOCK_HPP
#define CLOCK_HPP

class Clock {
public:
	Clock();
	void start();
	void restart();
	long elapsed();
private:
	long getTimeInMillis();
	long start_time;
};

#endif 