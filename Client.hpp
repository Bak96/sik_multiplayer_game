#ifndef CLIENT_HPP
#define CLIENT_HPP
#include "Datagram.hpp"
#include <vector>
#include <string>




class Client {
public:
	static const int BUFFER_LENGTH = 512;
	static const std::string DEFAULT_SERVER_PORT;
	static const std::string DEFAULT_GUI_PORT;
	static const std::string DEFAULT_GUI_HOST;
	Client(std::string name, std::string server_ip, std::string server_port,
		std::string gui_ip, std::string gui_port);
	void run();

private:
	void connectToServer();
	void connectToGui();

	int writeDataToBuffer(char *dest, ClientDatagram &data);
	void unpackServerMessage(char *buffer, size_t len);
	void unpackGuiMessage(char *buffer, size_t len, ClientDatagram &data);
	int readEvent(char *buffer, size_t len, uint32_t game_id);
	int addNewGameEvent(char *buffer, size_t len, uint32_t game_id);
	int addPixelEvent(char *buffer);
	int addPlayerEliminatedEvent(char *buffer);
	int addGameOverEvent();

	char buffer[BUFFER_LENGTH];
	int server_sock;
	int gui_sock;

	uint32_t game_id;
	bool playing;
	
	std::vector<std::string> events;
	size_t events_to_gui;
	std::vector<std::string> player_names;
	std::string name;
	std::string server_ip;
	std::string server_port;
	std::string gui_ip;
	std::string gui_port;
};

#endif