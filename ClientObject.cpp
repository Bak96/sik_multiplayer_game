#include "ClientObject.hpp"
#include <time.h>
#include <iostream>



ClientObject::ClientObject(struct sockaddr_in6 addr,uint64_t session_id, std::string &player_name)
: addr(addr)
, ready(false)
, is_playing(false)
, session_id(session_id)
, next_expected_event_no(0)
, player_name(player_name)
 {}

void ClientObject::setExpectedEventNo(uint32_t expected_no) {
	next_expected_event_no = expected_no;
}

uint32_t ClientObject::getSendingFrom() {
	return sendingFrom;
}

void ClientObject::setSendingFrom(uint32_t no) {
	sendingFrom = no;
}

void ClientObject::startSending() {
	sendingFrom = next_expected_event_no;
}

std::string &ClientObject::getName() {
	return player_name;
}

struct sockaddr_in6 & ClientObject::getAddr() {
	return addr;
}

void ClientObject::updateContactTime() {
	contact_time = time(NULL);
}

uint32_t ClientObject::getExpectedEventNo() {
	return next_expected_event_no;
}

time_t ClientObject::getContactTime() {
	return contact_time;
}

bool ClientObject::isReady() {
	return ready;
}

void ClientObject::setReady(bool r) {
	ready = r;
}

uint64_t ClientObject::getSessionId() {
	return session_id;
}

void ClientObject::setSessionId(uint64_t id) {
	session_id = id;
}

void ClientObject::setName(std::string name) {
	player_name = name;
}

bool ClientObject::isPlaying() {
	return is_playing;
}

void ClientObject::setPlaying(bool playing) {
	is_playing = playing;
}

bool ClientObject::sameAddr(struct sockaddr_in6 addr) {
	size_t addr_len = sizeof(addr.sin6_addr);
	for (size_t i = 0; i < addr_len; i++) {
		if (this->addr.sin6_addr.s6_addr[i] != addr.sin6_addr.s6_addr[i]) {
			return false;
		}
	}

	if (this->addr.sin6_port != addr.sin6_port) {
		return false;
	}
	
	return true;
	//&& getAddr().sin6_port == addr.sin6_port
}