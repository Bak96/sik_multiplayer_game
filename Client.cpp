#include "Client.hpp"
#include "Clock.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <endian.h>
#include <inttypes.h>
#include <boost/crc.hpp>
#include <iostream>
#include <sstream>

const std::string Client::DEFAULT_SERVER_PORT = "12345";
const std::string Client::DEFAULT_GUI_PORT = "12346";
const std::string Client::DEFAULT_GUI_HOST = "localhost";

Client::Client(std::string name, std::string server_ip, std::string server_port,
		std::string gui_ip, std::string gui_port)
: playing(false)
, name(name)
, server_ip(server_ip)
, server_port(server_port)
, gui_ip(gui_ip)
, gui_port(gui_port) {}

int Client::addGameOverEvent() {
	events.push_back("GAME_OVER");
	return 0;
}


void Client::unpackGuiMessage(char *buffer, size_t len, ClientDatagram &data) {
	std::string event(buffer, len);

	if (event == "LEFT_KEY_UP\n"
		|| event == "RIGHT_KEY_UP\n") {
		data.turn_direction = 0;
	} else if (event == "LEFT_KEY_DOWN\n") {
		data.turn_direction = -1;
	} else if (event == "RIGHT_KEY_DOWN\n") {
		data.turn_direction = 1;
	} else {
		fprintf(stderr, "client: ERROR unexpected datagram from gui\n");
		exit(1);
	}
}

int Client::addNewGameEvent(char *buffer, size_t len, uint32_t game_id) {
	std::ostringstream oss;

	int ptr = 0;
	uint32_t maxx;
	memcpy(&maxx, buffer+ptr, sizeof(maxx));
	maxx = be32toh(maxx);
	ptr += sizeof(maxx);

	uint32_t maxy;
	memcpy(&maxy, buffer+ptr, sizeof(maxy));
	maxy = be32toh(maxy);
	ptr += sizeof(maxy);

	oss << "NEW_GAME " << maxx << " " << maxy;
	player_names = std::vector<std::string>();

	while (ptr < (int)len) {
		int name_length = strlen(buffer + ptr);
		//TODO trzeba zbadac czy znaki ascii sie zgadzaja
		if (name_length > 64) {
			return -1;
		}

		std::string player_name(buffer+ptr, name_length);
		player_names.push_back(player_name);
		oss << " " << player_name;
		ptr += name_length + 1;
	}

	oss << "\n";
	events = std::vector<std::string>();
	events.push_back(oss.str());
	events_to_gui = 0;
	this->game_id = game_id;
	playing = true;

	return len;
}

int Client::addPixelEvent(char *buffer) {
	std::ostringstream oss;

	int ptr = 0;
	uint8_t player_number;
	memcpy(&player_number, buffer+ptr, sizeof(player_number));
	ptr += sizeof(player_number);

	uint32_t x;
	memcpy(&x, buffer+ptr, sizeof(x));
	x = be32toh(x);
	ptr += sizeof(x);

	uint32_t y;
	memcpy(&y, buffer+ptr, sizeof(y));
	y = be32toh(y);
	ptr += sizeof(y);

	oss << "PIXEL " << x << " " << y << " " << player_names[player_number] << "\n";
	events.push_back(oss.str());

	return ptr;
}
int Client::addPlayerEliminatedEvent(char *buffer) {
	std::ostringstream oss;

	int ptr = 0;
	uint8_t player_number;

	memcpy(&player_number, buffer+ptr, sizeof(player_number));
	ptr += sizeof(player_number);

	oss << "PLAYER_ELIMINATED " << player_names[player_number] << "\n";
	events.push_back(oss.str());
	return ptr;
}

int Client::readEvent(char *buffer, size_t len, uint32_t game_id) {
	//dostaje bufor pelen eventow. buffer wskazuje na event ktory mam wyciagnac
	int ptr = 0;

	uint32_t event_len;
	memcpy(&event_len, buffer, sizeof(event_len));
	
	event_len = be32toh(event_len);
	ptr += sizeof(event_len);
	
	ptr += event_len; //jmp to crc32
	uint32_t crc32;

	memcpy(&crc32, buffer + ptr, sizeof (crc32));
	crc32 = be32toh(crc32);
	
	//crc32
	boost::crc_32_type result;
	result.process_bytes(buffer, event_len + sizeof(event_len));
	if (result.checksum() != crc32) {
		return -1;
	}

	ptr = 0 + sizeof(event_len);
	uint32_t event_no;
	memcpy(&event_no, buffer + ptr, sizeof(event_no));
	event_no = be32toh(event_no);

	if (event_no != events.size()) {
		return -1;
	}

	ptr += sizeof(event_no);
	//trzeba ogarnac czy stare czy nowe
	uint8_t event_type;
	memcpy(&event_type, buffer + ptr, sizeof(event_type));
	ptr += sizeof (event_type);

	int event_data_len; // > 0
	if (event_type == 0) {
		event_data_len = addNewGameEvent(buffer + ptr, event_len - 5, game_id);
		//std::cout << "add new game event" << std:: endl;
		return -1;
	} else if (event_type == 1) {
		event_data_len = addPixelEvent(buffer + ptr);
	} else if (event_type == 2) {
		event_data_len = addPlayerEliminatedEvent(buffer + ptr);
		//std::cout << "add player eliminated event" << std:: endl;
	} else if (event_type == 3) {
		addGameOverEvent();
		//printf("GAME OVER\n");
		event_data_len = 0;
	} else {
		//printf("nie znam typu eventu\n");
		event_data_len = -1;
	}

	if (event_data_len < 0) {
		return -1;
	}

	return sizeof(event_len) + event_len + sizeof(crc32);
}

void Client::unpackServerMessage(char *buffer, size_t len) {
	//game_id (4 bytes)
	//events (???)

	if (len < 4) {
		return;
	}

	int ptr = 0;
	uint32_t game_id;
	memcpy(&game_id, buffer + ptr, sizeof(game_id));

	//dostalem stary komunikat
	if (!playing && game_id == this->game_id) {
		return;
	}

	if (playing && game_id != this->game_id) {
		return;
	}

	ptr += sizeof(game_id);

	while(ptr < (int)len) {
		int event_size = readEvent(buffer + ptr, len - ptr, game_id);
		if (event_size < 0) {
			return;
		}
		ptr += event_size;
	}

}

int Client::writeDataToBuffer(char *dest, ClientDatagram &data) {
	int ptr = 0;
	uint64_t session_id = htobe64(data.session_id);
	memcpy(dest + ptr, &session_id, sizeof(session_id)); //session_id
	ptr += sizeof(session_id);

	dest[ptr] = data.turn_direction; //turn_direction
	ptr += 1;

	uint32_t next_expected_event_no = htobe32(data.next_expected_event_no);
	//next_expected_no
	memcpy(dest + ptr, &next_expected_event_no, sizeof(next_expected_event_no));
	ptr += sizeof(next_expected_event_no);

	const char *name = data.player_name.data();
	memcpy(dest + ptr, name, data.player_name.length()); //player_name
	ptr += data.player_name.length();

	return ptr;
}

void Client::connectToServer() {
	struct addrinfo hints, *servinfo, *p;
	char s[INET_ADDRSTRLEN];
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	if ((rv = getaddrinfo(server_ip.c_str(), server_port.c_str(), &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
	}

	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((server_sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
	//		fprintf(stderr, "client: socket");
			continue;
		}

		if (connect(server_sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(server_sock);
	//		fprintf(stderr, "client: connect");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect server\n");
		exit(1);
	}

	inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr),
		s, sizeof s);

	printf("client: connecting to %s\n", s);
	freeaddrinfo(servinfo);
}

void Client::connectToGui() {
	struct addrinfo hints, *servinfo, *p;
	char s[INET_ADDRSTRLEN];
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(gui_ip.c_str(), gui_port.c_str(), &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
	}

	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((gui_sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
		//	fprintf(stderr, "client: socket\n");
			continue;
		}

		if (connect(gui_sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(gui_sock);
			//fprintf(stderr, "client: connect\n");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect gui\n");
		exit(1);
	}

	inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr),
		s, sizeof s);

	printf("client: connecting to %s\n", s);
	freeaddrinfo(servinfo);
}

void Client::run() {
	ClientDatagram data;
	fd_set master, read_fds, master_write, write_fds;
	fd_set *write_fds_ptr;
	struct timeval timeout;

	int rcv_flags, sflags;
	int rv;
	ssize_t snd_len, rcv_len;
	size_t data_len = 0;
	memset(buffer, 0, sizeof(buffer));

	struct timeval t;
    gettimeofday(&t, NULL);
    data.session_id = t.tv_sec * 1000000 + t.tv_usec;
	data.turn_direction = 0;
	data.next_expected_event_no = 0;
	data.player_name = name;

	sflags = 0;

	connectToServer();
	connectToGui();
	
	FD_SET(server_sock, &master);
	FD_SET(gui_sock, &master);
	FD_SET(gui_sock, &master_write);


	int highest_num_sock = server_sock;
	if (gui_sock > highest_num_sock) {
		highest_num_sock = gui_sock;
	}


	Clock clock;
	long time_elapsed = 0;
	long time_debug = 0;
	events_to_gui = 0;

	while (1) {
		time_elapsed += clock.elapsed();
		time_debug += clock.elapsed();
		clock.restart();
		//co 20ms wysylaj datagram do serwera
		while (time_elapsed >= 20) {
			data.next_expected_event_no = events.size();
			data_len = writeDataToBuffer(buffer, data);
			snd_len = send(server_sock, buffer, data_len, sflags);

			if (snd_len != (int)data_len) {
				fprintf(stderr, "client: error on sending datagram\n");
			}
			time_elapsed -= 20;

		}
		
		if (events_to_gui < events.size()) {
			if (events[events_to_gui] != "GAME_OVER") {
				const char *buf = events[events_to_gui].data();
				data_len = events[events_to_gui].length();
				snd_len = send(gui_sock, buf, data_len, sflags);

				if (snd_len != (int)data_len) {
					fprintf(stderr, "client: error on sending datagram to gui\n");
				} else {
					events_to_gui++;
				}
			} else {
				playing = false;
				events = std::vector<std::string>();
			}
		}
		
		read_fds = master;
		if (events_to_gui < events.size()) {
			write_fds = master_write;
			write_fds_ptr = &write_fds;
		} else {
			write_fds_ptr = NULL; //czasami nie chce tam pisac
		}


		timeout.tv_sec = 0;
		timeout.tv_usec = 20000 - time_elapsed * 1000;

		rv = select(highest_num_sock+1, &read_fds, write_fds_ptr, NULL, &timeout);

		if (rv == -1) {
			fprintf(stderr, "Error: select");
			exit(1);
		} else if (rv == 0) {
			//printf("timeout \n");
		} else if (rv > 0) {
			//ktos do nas napisal
			if (FD_ISSET(server_sock, &read_fds)) {
				rcv_len = recv(server_sock, buffer, sizeof(buffer), rcv_flags);
				
				if (rcv_len < 0) {
				//	fprintf(stderr, "error on receiving datagram\n");
				} else {
					unpackServerMessage(buffer, rcv_len);
				}
			} else if(FD_ISSET(gui_sock, &read_fds)) {
				rcv_len = recv(gui_sock, buffer, sizeof(buffer), rcv_flags);

				if (rcv_len < 0) {
					//fprintf(stderr, "error on receiving datagram\n");
				} else {
					unpackGuiMessage(buffer, rcv_len, data);
				}
			}

			if (FD_ISSET(gui_sock, &write_fds)) {
				if (events_to_gui < events.size()) {
					if (events[events_to_gui] != "GAME_OVER") {
						const char *buf = events[events_to_gui].data();
						data_len = events[events_to_gui].length();
						snd_len = send(gui_sock, buf, data_len, sflags);

						if (snd_len != (int)data_len) {
							fprintf(stderr, "client: error on sending datagram to gui\n");
						} else {
							events_to_gui++;
						}
					} else {
						playing = false;
						events = std::vector<std::string>();
					}
				}

			}
		}

	}
}