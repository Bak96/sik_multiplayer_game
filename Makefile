TARGET: siktacka-server siktacka-client
CXX=g++
CXXFLAGS=-Wall -std=c++14 -O2 
DEPS=
OBJ= Server.o Game.o ClientObject.o Clock.o Player.o RandomGenerator.o Client.o

siktacka-server: $(OBJ) main_server.o
	$(CXX) $(CXXFLAGS) -o $@ $^

siktacka-client: $(OBJ) main_client.o
	$(CXX) $(CXXFLAGS) -o $@ $^
	
.PHONY: clean

clean:
	rm *.o
	rm siktacka-server
	rm siktacka-client
