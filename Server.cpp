#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <endian.h>
#include <inttypes.h>
#include <sys/time.h>
#include <cassert>
#include <boost/crc.hpp>
#include <iostream>


#include "Server.hpp"
#include "Game.hpp"
#include "Clock.hpp"
#include "Datagram.hpp"

//const int Server::BUFFER_LEGTH = 512;
const std::string Server::DEFAULT_PORT = "12345";
const int Server::DEFAULT_WIDTH = 800;
const int Server::DEFAULT_HEIGHT = 600;
const int Server::DEFAULT_ROUNDS_PER_SEC = 50;
const int Server::DEFAULT_TURNING_SPEED = 6;
const int Server::TIMEOUT_S = 2;
const int Server::MAX_CLIENTS = 42;

Server::Server(int width, int height, std::string port,
	int rounds_per_sec, int turning_speed, uint64_t seed)
: game(width, height, turning_speed, seed)
, port(port) {
	time_per_frame = 1000 / rounds_per_sec;
	status = IN_LOBBY;
}



bool Server::isNameFree(std::string name) {
	if (name == "") {
		return true;
	}

	for (auto it = clients.begin(); it != clients.end(); it++) {
		ClientObject &client = *it;
		if (client.getName() != "" && client.getName() == name)	{
			return false;
		}
	}

	return true;
}

void Server::removeClientsWithNoConnection() {
	time_t actual_time = time(NULL);

	for (auto it = clients.begin(); it != clients.end(); it++) {
		ClientObject &client = *it;
		if (actual_time - client.getContactTime() > TIMEOUT_S) {
			it = clients.erase(it);
			if (it != clients.begin()) {
				it--;
			}
		}
	}
}

bool Server::readClientDatagram(char *buffer, ssize_t len, struct ClientDatagram &data) {
	//session_id (8 bytes)
	//turn_direction (1 byte)
	//next_expected_event_no (4 bytes)
	//player_name (0 - 64 bytes)
	if (len > 77 || len < 13) return false;

	uint64_t session_id;
	int8_t turn_direction;
	uint32_t next_expected_event_no;
	std::string player_name;
	int ptr = 0;

	memcpy(&session_id, buffer + ptr, sizeof(session_id));
	data.session_id = be64toh(session_id);
	ptr += sizeof(session_id);

	memcpy(&turn_direction, buffer + ptr, sizeof(turn_direction));
	data.turn_direction = turn_direction;
	ptr += sizeof(turn_direction);

	memcpy(&next_expected_event_no, buffer + ptr, sizeof(next_expected_event_no));
	data.next_expected_event_no = be32toh(next_expected_event_no);
	ptr += sizeof(next_expected_event_no);

	if (ptr >= len) {
		player_name = "";
	} else {
		for (int i = ptr; i < len; i++) {
			if (buffer[i] < 33 || buffer[i] > 126) {
				return false;
			}
		}

		data.player_name = std::string(buffer + ptr, len - ptr);
	}

	return true;
}

void Server::sendEvents(ClientObject &client) {
	int snd_flags;
	ssize_t snd_len;
	int data_len = 0;
	uint32_t game_id_net = htobe32(game.get_id());

	socklen_t snda_len = sizeof(client.getAddr());
	snd_flags = 0;

	uint32_t event_no = client.getSendingFrom();

	if (event_no < events.size()) {	
		memcpy(buffer + data_len, &game_id_net, sizeof(game_id_net));
		data_len += sizeof(game_id_net);
		while (event_no < events.size() &&
			data_len + events[event_no].len <= BUFFER_LENGTH) {
			
			memcpy(buffer + data_len, events[event_no].data, events[event_no].len);
			data_len += events[event_no].len;
			event_no++;
		}
		
		snd_len = sendto(sock, buffer, (size_t) data_len, snd_flags,
			(struct sockaddr *) &client.getAddr(), snda_len);
		if (snd_len != data_len)
			fprintf(stderr, "error on sending datagram to client socket");
		data_len = 0;

		client.setSendingFrom(event_no);
	}
}

void Server::serveClient(char *buffer, ssize_t buf_len, struct sockaddr_in6 *their_addr, socklen_t rcva_len) {
	removeClientsWithNoConnection();
	
	struct ClientDatagram data;
	if (!readClientDatagram(buffer, buf_len, data)) {
		return;
	}

	//petla przechodzi po podlaczonych klientach. szukam, czy pakiet nie pochodzi
	//od ktoregos z nich
	for (auto it = clients.begin(); it != clients.end(); it++) {
		ClientObject &client = *it;

		if (client.sameAddr(*their_addr)) {
			if (data.session_id < client.getSessionId()){
				return; //stary datagram, dropam pakiet
			} 

			if (data.session_id > client.getSessionId()) { //nowszy. trzeba odpiac starego
				client.setPlaying(false);
				client.setReady(false);
				client.setSessionId(data.session_id);
				client.setName(""); //TODO !!
				//jesli nie moge zajac tego imienia, to trzeba zwolnic miejsce w serwerze
				if (!isNameFree(data.player_name)) {
					it = clients.erase(it);
					return;
				}
				client.setName(data.player_name);

			} else if (client.isPlaying()){ //session OK
				if (client.getName() != data.player_name) {
					return; //odrzucam pakiet, bo cos imiona sie nie zgadzaja
				}
				if (data.turn_direction != 0 && data.turn_direction != 1
				 && data.turn_direction != -1) {
					return; //odrzucam pakiet, bo zly turn_direction
				}
				if (data.next_expected_event_no < client.getExpectedEventNo()) {
					return; //odrzucam pakiet, bo mniejszy 
				}


				game.set_turn_direction(data.player_name, data.turn_direction);	
			}
			

			client.updateContactTime();
			client.setExpectedEventNo(data.next_expected_event_no);

			if (data.turn_direction != 0) {
				client.setReady(true);
			}

			client.startSending();

			return;
		}
	}

	//dodaj nowego klienta jesli jest miejsce i nie ma juz takiego samego nicku
	if (clients.size() < MAX_CLIENTS && isNameFree(data.player_name)) {
		ClientObject co(*their_addr, data.session_id, data.player_name);
		co.updateContactTime();
		co.setExpectedEventNo(data.next_expected_event_no);
		co.startSending();
		clients.push_back(co);		
	}
	
}

bool Server::clientsReadyToPlay() {
	int players_ready = 0;
	for (ClientObject &client : clients)
	{
		if (client.getName() == "")	continue;

		if(!client.isReady()) {
			return false;
		} else {
			players_ready++;
		}
	}

	if (players_ready > 1) {
		return true;	
	} else {
		return false;
	}
}

void Server::saveNewGameEvent(Game::Event &event, int event_no){
	//len (4 bytes)
	//event_no (4 bytes)
	//event_type (1 byte)
	//maxx (4 bytes)
	//maxy (4 bytes)
	//player_list (? bytes)
	//crc32 (4 bytes)
	Data data;
	int names_len = 0;
	//17 bajtow na pierdoly + 4 bajty na crc 	//491 na imiona
	for (auto it = player_names.begin(); it != player_names.end(); it++) {
		std::string &name = *it;
		names_len += name.length() + 1;
	}

	data.len = 21 + names_len;
	data.data = new char[data.len];

	int ptr = 0;
	uint32_t len = htobe32(13 + names_len); //sumaryczna dlugosc pol event_*
	memcpy(data.data + ptr, &len, sizeof(len));
	ptr += sizeof(len);

	uint32_t number = htobe32(event_no);
	memcpy(data.data + ptr, &number, sizeof(number)); //event_no
	ptr += sizeof(number);

	data.data[ptr] = 0; //event type
	ptr++;
	uint32_t maxx = htobe32(DEFAULT_WIDTH);
	memcpy(data.data + ptr, &maxx, sizeof(maxx)); //maxx
	ptr += sizeof(maxx);
	uint32_t maxy = htobe32(DEFAULT_HEIGHT); 
	memcpy(data.data + ptr, &maxy, sizeof(maxy)); //maxy
	ptr += sizeof(maxy);

	//lista nazw graczy
	for (auto it = player_names.begin(); it != player_names.end(); it++) {
		std::string &name = *it;
		const char *buf = name.c_str();
		memcpy(data.data + ptr, buf, name.length());
		ptr += name.length();
		data.data[ptr] = '\0';
		ptr++;
	}

	//crc32
	boost::crc_32_type result;
    result.process_bytes(data.data, data.len - 4);
    uint32_t crc32 = htobe32(result.checksum());

    memcpy(data.data + ptr, &crc32, sizeof(crc32)); // crc32

    events.push_back(data);
}

void Server::savePixelEvent(Game::Event &event, int event_no) {
	//len (4 bytes)
	//event_no (4 bytes)
	//event_type (1 byte)
	//player_number (1 byte)
	//x (4 bytes)
	//y (4 bytes)
	//crc32 (4 bytes)
	Data data;
	data.len = 22;
	data.data = new char[data.len];
	
	int ptr = 0;
	uint32_t len = htobe32(14); //sumaryczna dlugosc pol event_*
	memcpy(data.data + ptr, &len, sizeof(len));
	ptr += sizeof(len);

	uint32_t number = htobe32(event_no);
	memcpy(data.data + ptr, &number, sizeof(number)); //event_no
	ptr += sizeof(number);

	data.data[ptr] = 1; //event_type
	ptr++;

	data.data[ptr] = event.player_number; //player_number
	ptr++;

	uint32_t x = htobe32(event.x);
	memcpy(data.data + ptr, &x, sizeof(x)); //x
	ptr += sizeof(x);
	uint32_t y = htobe32(event.y); 
	memcpy(data.data + ptr, &y, sizeof(y)); //y
	ptr += sizeof(y);

	//crc32
	boost::crc_32_type result;
    result.process_bytes(data.data, data.len - 4);
    uint32_t crc32 = htobe32(result.checksum());

    memcpy(data.data + ptr, &crc32, sizeof(crc32)); // crc32

    events.push_back(data);
}

void Server::savePlayerEliminatedEvent(Game::Event &event, int event_no) {
	//len (4 bytes)
	//event_no (4 bytes)
	//event_type (1 byte)
	//player_number (1 byte)
	//crc32 (4 bytes)
	Data data;
	data.len = 14;
	data.data = new char[data.len];

	int ptr = 0;
	uint32_t len = htobe32(6); //sumaryczna dlugosc pol event_*
	memcpy(data.data + ptr, &len, sizeof(len));
	ptr += sizeof(len);

	uint32_t number = htobe32(event_no);
	memcpy(data.data + ptr, &number, sizeof(number)); //event_no
	ptr += sizeof(number);

	data.data[ptr] = 2; //event_type
	ptr++;

	data.data[ptr] = event.player_number; //player_number
	ptr++;

	//crc32
	boost::crc_32_type result;
    result.process_bytes(data.data, data.len - 4);
    uint32_t crc32 = htobe32(result.checksum());

    memcpy(data.data + ptr, &crc32, sizeof(crc32)); // crc32

    events.push_back(data);
}
void Server::saveGameOverEvent(Game::Event &event, int event_no) {
	//len (4 bytes)
	//event_no (4 bytes)
	//event_type (1 byte)
	//crc32 (4 bytes)
	Data data;
	data.len = 13;
	data.data = new char[data.len];

	int ptr = 0;
	uint32_t len = htobe32(5); //sumaryczna dlugosc pol event_*
	memcpy(data.data + ptr, &len, sizeof(len));
	ptr += sizeof(len);

	uint32_t number = htobe32(event_no);
	memcpy(data.data + ptr, &number, sizeof(number)); //event_no
	ptr += sizeof(number);

	data.data[ptr] = 3; //event_type
	ptr++;
	
	//crc32
	boost::crc_32_type result;
    result.process_bytes(data.data, data.len - 4);
    uint32_t crc32 = htobe32(result.checksum());

    memcpy(data.data + ptr, &crc32, sizeof(crc32)); // crc32

    events.push_back(data);
}

void Server::saveEvents(std::list<Game::Event> &game_events) {

	for (auto it = game_events.begin(); it != game_events.end(); it++) {
		Game::Event &game_event = *it;
		switch(game_event.type) {
			case Game::Event::NEW_GAME :
				saveNewGameEvent(game_event, events.size());
				break;
			case Game::Event::PIXEL :
				savePixelEvent(game_event, events.size());
				break;
			case Game::Event::PLAYER_ELIMINATED :
				savePlayerEliminatedEvent(game_event, events.size());
				break;
			case Game::Event::GAME_OVER :
				saveGameOverEvent(game_event, events.size());
				status = IN_LOBBY;
				for (auto it = clients.begin(); it != clients.end(); it++) {
					ClientObject &client = *it;
					client.setReady(false);
					client.setPlaying(false);
				}

				break;
			default:
				assert(false);
		}
	}
}

int Server::createBindSocket(const char* port) {
	int sock;
	int rv;
	struct addrinfo hints, *ai, *p;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	if ((rv = getaddrinfo(NULL, port, &hints, &ai)) != 0) {
		fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
		exit(1);
	}

	for (p = ai; p != NULL; p = p->ai_next) {
		sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		int flag = 0;
		setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (void *) &flag, sizeof(flag));

		if (sock < 0) {
			continue;
		}

		if (bind(sock, p->ai_addr, p->ai_addrlen) < 0) {
			close(sock);
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "selectserver: failed to bind\n");
		exit(1);
	}
// 	fprintf(stderr, "port: %d\n",ntohs(((struct sockaddr_in*)(p->ai_addr))->sin_port));
	freeaddrinfo(ai);
	return sock;
}

void Server::createPlayerList() {
	int max_length = 512 - 21; //zeby imiona sie zmiescily w naglowku
	int len = 0;
	player_names = std::list<std::string>();

	for (ClientObject &client : clients) {
		if (client.getName() != "") {
			len += client.getName().length() + 1;
			if (len <= max_length) {
				player_names.push_back(client.getName());
				client.setPlaying(true);
			} else {
				break;
			}
		}

	}

	player_names.sort();
}

void Server::printPlayersInfo() {
	int i = 0;
	printf("connected clients: %ld\n", clients.size());
	for (ClientObject &client : clients) {
		std::cout << i << " ";
		if (client.getName() == "") {
			std::cout << "noname";
		} else {
			std::cout << client.getName();
		}
		std::cout << " ready " <<  client.isReady();
		std::cout << " session " << std::endl;

		i++;
	}


}

void Server::printStatus() {
	if (status == IN_GAME) {
		std::cout << "status: IN GAME" << std::endl;
	} else if (status == IN_LOBBY) {
		std::cout << "status: IN LOBBY" << std::endl;
	} else {
		std::cout << "status: uknknown" << std::endl;
	}
}

void Server::run() {
	struct timeval timeout;
	
	//char remoteIP[INET6_ADDRSTRLEN];
	
	struct sockaddr_in6 their_address;
	int rcv_flags;
	int rv;
	ssize_t rcv_len;
	socklen_t rcva_len;
	

	fd_set master, read_fds, write_fds;
	fd_set *write_fds_ptr;

	sock = createBindSocket(port.c_str());

	FD_SET(sock, &master);

	rcv_flags = 0;
	rcva_len = (socklen_t) sizeof(their_address);

	Clock clock;
	
	long time_elapsed = 0;
	long time_to_debug = 0;
	size_t client_to_serve = 0;
	for (;;) {
		time_elapsed += clock.elapsed();
		time_to_debug += clock.elapsed();
		clock.restart();
/*
		if (time_to_debug > 5000) {
			time_to_debug = 0;
			std::cout << std::endl << std::endl;
			removeClientsWithNoConnection();
			printStatus();

			printPlayersInfo();
			std::cout << "events_num: " << events.size() << std::endl;
		}
*/

		while (status == IN_GAME && time_elapsed > time_per_frame) {
			std::list<Game::Event> game_events = game.tick();
			saveEvents(game_events);

			for (auto it = clients.begin(); it != clients.end(); it++) {
				ClientObject &client = *it;
				client.startSending();
			}

			time_elapsed -= time_per_frame;
		}

		//na razie pierwsza lipna strategia wysylania/odbierania
		read_fds = master;
		write_fds = master;

		timeout.tv_sec = 0;

		if (status == IN_LOBBY) {
			timeout.tv_usec = 1000;
		} else {
			timeout.tv_usec = time_per_frame * 1000 - time_elapsed * 1000;
			if (timeout.tv_usec < 0) {
				timeout.tv_usec = 0;
			}
		}
		
		write_fds_ptr = NULL;
		//sprawdzam tutaj czy w ogole warto czekac na socket pisajacy
		for (auto it = clients.begin(); it != clients.end(); it++) {
			ClientObject &client = *it;
			if (client.getSendingFrom() < events.size()) {
				write_fds_ptr = &write_fds;
				break;
			}
		}


		rv = select(sock+1, &read_fds, write_fds_ptr, NULL, &timeout);
		if (rv == -1) {
			fprintf(stderr, "Error: select");
			exit(1);
		} else if (rv == 0) {
			//printf("timeout \n");
		} else if (rv > 0) {
			//ktos do nas napisal
			if (FD_ISSET(sock, &read_fds)) {
				rcv_len = recvfrom(sock, buffer, sizeof(buffer), rcv_flags,
					(struct sockaddr *) &their_address, &rcva_len);
				if (rcv_len < 0) {
					fprintf(stderr, "error on receiving datagram\n");
				} else {
					/*fprintf(stderr, "server: receive datagram %s:%d\n",
								inet_ntop(their_address.sin6_family, &their_address.sin6_addr,
									remoteIP, INET6_ADDRSTRLEN),ntohs(their_address.sin6_port));
					*/
					removeClientsWithNoConnection();
					
					serveClient(buffer, rcv_len, &their_address, rcva_len);

					if (status == IN_LOBBY && clients.size() > 1 && clientsReadyToPlay()) {
						events = std::vector<Data>();
						createPlayerList();
						std::list<Game::Event> game_events = game.start_game(player_names);
						saveEvents(game_events);
						
						status = IN_GAME;
						time_elapsed = 0;
						clock.restart();
					}
				}
			}

			if (write_fds_ptr != NULL && clients.size() > 0 
				&& FD_ISSET(sock, &write_fds)) {
				if (client_to_serve > clients.size()) {
					client_to_serve = 0;
				}
				auto it = clients.begin();
				std::advance(it, client_to_serve);
				ClientObject &client = *it;
				sendEvents(client);
				client_to_serve++;
			}
		}
	}
}