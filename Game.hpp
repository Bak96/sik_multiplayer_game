#ifndef GAME_HPP
#define GAME_HPP

#include<list>
#include<vector>
#include<map>
#include<string>

#include "RandomGenerator.hpp"
#include "Player.hpp"

class Game {
public:
	struct Event {

		enum Type {
			NEW_GAME,
			PIXEL,
			PLAYER_ELIMINATED,
			GAME_OVER
		};
		Type type;
		int player_number;
		int x;
		int y;
	};


	Game(int width, int height, int turning_speed, int seed);
	std::list<Event> start_game(std::list<std::string> &start_player_names);
	void set_turn_direction(std::string name, int8_t turn_direction);
	uint32_t get_id();
	void print_players();
	std::list<Event> tick();
private:
	int width;
	int height;
	int turning_speed;
	int seed;
	
	uint32_t game_id;
	std::list<std::string> player_names;
	std::map<std::string, Player> players;
	std::vector<std::vector<char>> map; //2d array
	int players_left;

	RandomGenerator random;
};

#endif //GAME_HPP