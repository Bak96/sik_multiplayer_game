#include "RandomGenerator.hpp"

RandomGenerator::RandomGenerator(long seed) : seed(seed)
, firstTime(true) {}
long RandomGenerator::rand() {
	long ret;
	if (firstTime) {
		firstTime = false;
		return seed;
	} 

	ret = ((seed) * 279470273) % 4294967291;
	seed = ret;
	return ret;
}