#ifndef SERVER_HPP
#define SERVER_HPP

#include <list>
#include <vector>
#include "ClientObject.hpp"
#include "Datagram.hpp"
#include "Game.hpp"
#include <string>

class Server {
public:
	static const int BUFFER_LENGTH = 512;
	static const std::string DEFAULT_PORT;
	static const int DEFAULT_WIDTH;
	static const int DEFAULT_HEIGHT;
	static const int DEFAULT_ROUNDS_PER_SEC;
	static const int DEFAULT_TURNING_SPEED;
	static const int TIMEOUT_S;
	static const int MAX_CLIENTS;

	enum Status {
		IN_LOBBY,
		IN_GAME
	};

	Server(int width, int height, std::string port, 
		int rounds_per_sec, int turning_speed, uint64_t seed);
	void run();
private:
	int createBindSocket(const char* port);
	void saveEvents(std::list<Game::Event> &game_events);
	void saveNewGameEvent(Game::Event &event, int event_no);
	void savePixelEvent(Game::Event &event, int event_no);
	void savePlayerEliminatedEvent(Game::Event &event, int event_no);
	void saveGameOverEvent(Game::Event &event, int event_no);
	bool readClientDatagram(char *buffer, ssize_t len, struct ClientDatagram &data);
	void serveClient(char *buffer, ssize_t rcv_len, struct sockaddr_in6 *their_addr, socklen_t rcva_len);
	void removeClientsWithNoConnection();
	void sendEvents(ClientObject &client);
	void createPlayerList();
	bool clientsReadyToPlay();
	bool isNameFree(std::string name);

	void printPlayersInfo();
	void printStatus();

	long time_per_frame;

	std::list<ClientObject> clients;
	std::vector<Data> events;
	std::list<std::string> player_names;
	Status status;
	Game game;
	int sock;
	char buffer[BUFFER_LENGTH];
	std::string port;

};


#endif