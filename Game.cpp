#include "Game.hpp"
#include <iostream>

Game::Game(int width, int height, int turning_speed, int seed)
: width(width)
, height(height)
, turning_speed(turning_speed)
, seed(seed)
, random(seed) {
	map.resize(height);
	for (int i = 0; i < height; i++) {
		map[i].resize(width);
	}
}

uint32_t Game::get_id() {
	return game_id;
}

void Game::set_turn_direction(std::string name, int8_t turn_direction) {
	players[name].setTurnDirection(turn_direction);
}

std::list<Game::Event> Game::start_game(std::list<std::string> &start_player_names) {
	Game::Event event;
	std::list<Game::Event> events;

	player_names = start_player_names;
	player_names.sort();

	players_left = player_names.size();

	event.type = Event::NEW_GAME;
	events.push_back(event);
	game_id = random.rand();


	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			map[j][i] = 0;
		}
	}

	int i = 0;
	for (auto it = player_names.begin(); it != player_names.end() && players_left > 1; it++, i++) {
		double x = (random.rand() % width) + 0.5;
		double y = (random.rand() % height) + 0.5;
		int direction = random.rand() % 360;
		players[*it] = Player(*it, x, y, direction);

		//jesli pixle zajete, to generuj player eliminated
		if (map[(int)y][(int)x] != 0) {
			event.type = Event::PLAYER_ELIMINATED;
			event.player_number = i;
			players[*it].setAlive(false);

			events.push_back(event);
		} else { //jesli pixel wolny, to ustaw tam playera
			map[(int)y][(int)x] = 1;
			event.type = Event::PIXEL;
			event.x = (int) x;
			event.y = (int) y;
			events.push_back(event);
		}

	}

	if (players_left == 1) {
		event.type = Event::GAME_OVER;
		events.push_back(event);
	}

	return events;
}

std::list<Game::Event> Game::tick() {
	Game::Event event;
	std::list<Game::Event> events;

	int i = 0;
	for (auto it = player_names.begin(); it != player_names.end() && players_left > 1; it++, i++) {
		Player &player = players[*it];
		if (!player.isAlive())
			continue;

		player.updateDirection(turning_speed);
		if (!player.moveForward())
			continue;
		
		int x = player.getX();
		int y = player.getY();
		

		if (x < 0 || x >= width || y < 0 || y >= height || map[y][x] != 0) {
			event.type = Event::PLAYER_ELIMINATED;
			event.player_number = i;
			player.setAlive(false);
			players_left--;

			events.push_back(event);
		} else {
			map[y][x] = 1;
			event.type = Event::PIXEL;
			event.x = x;
			event.y = y;
			event.player_number = i;
			events.push_back(event);
		}
	}

	if (players_left == 1) {
		event.type = Event::GAME_OVER;
		events.push_back(event);
	}

	return events;
}

void Game::print_players() {
	for (auto it = player_names.begin(); it != player_names.end(); it++) {
		std::cout << *it << std::endl;
	}
}